/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mandelbrotset;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.awt.Image;
import java.math.BigDecimal;
import java.math.MathContext;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class MandelbrotSet extends JFrame implements KeyListener {

	protected static final MathContext MATH_CONTEXT = new MathContext(5);
    protected static final BigDecimal IMAGE_WIDTH = new BigDecimal(480, MATH_CONTEXT);
    protected static final BigDecimal IMAGE_HEIGHT = new BigDecimal(360, MATH_CONTEXT);
    private static final BigDecimal ZOOM_FACTOR = new BigDecimal("0.5", MATH_CONTEXT);
    private static final BigDecimal OFFSET_FACTOR = new BigDecimal(160, MATH_CONTEXT);
	private static final BigDecimal CORE_COUNT = new BigDecimal(Runtime.getRuntime().availableProcessors());
    private Image image;
    private BigDecimal offsetX;
    private BigDecimal offsetY;
    private BigDecimal zoom;
    private boolean stateChanged = true;

    public MandelbrotSet(String title){
        super(title);
        image = new BufferedImage(IMAGE_WIDTH.intValue(), IMAGE_HEIGHT.intValue(), BufferedImage.TYPE_INT_ARGB);
        offsetX = new BigDecimal("-0.7", MATH_CONTEXT);
        offsetY = new BigDecimal("1.0", MATH_CONTEXT);
        zoom = new BigDecimal("0.0009", MATH_CONTEXT);
        addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Nothing here yet
    }

    @Override
    public void keyPressed(KeyEvent e) {
        stateChanged = true;
        if(e.getKeyCode() == KeyEvent.VK_W) {
            //offsetY -= OFFSET_FACTOR * zoom;
			offsetY = offsetY.subtract(zoom.multiply(OFFSET_FACTOR));
            repaint();
        }
        if(e.getKeyCode() == KeyEvent.VK_A) {
            //offsetX -= OFFSET_FACTOR * zoom;
			offsetX = offsetX.subtract(zoom.multiply(OFFSET_FACTOR));
            repaint();
        }
        if(e.getKeyCode() == KeyEvent.VK_S) {
            //offsetY += OFFSET_FACTOR * zoom;
			offsetY = offsetY.add(zoom.multiply(OFFSET_FACTOR));
            repaint();
        }
        if(e.getKeyCode() == KeyEvent.VK_D) {
            //offsetX += OFFSET_FACTOR * zoom;
			offsetX = offsetX.add(zoom.multiply(OFFSET_FACTOR));
            repaint();
        }
        if(e.getKeyCode() == KeyEvent.VK_EQUALS) {
            //zoom *= ZOOM_FACTOR;
			zoom = zoom.multiply(ZOOM_FACTOR);
            repaint();
        }
        if(e.getKeyCode() == KeyEvent.VK_MINUS) {
            //zoom /= ZOOM_FACTOR;
			zoom = zoom.divide(ZOOM_FACTOR);
            repaint();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // Nothing here yet
    }

    private static void createAndShowGUI() {
        MandelbrotSet ms = new MandelbrotSet("Mandelbrot Set");
        ms.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        ms.getContentPane().add(ms.new MandelbrotPane());
        ms.setSize(IMAGE_WIDTH.intValue(), IMAGE_HEIGHT.intValue());
        ms.setResizable(false);
        ms.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
			createAndShowGUI();
		});
    }

    private class MandelbrotPane extends JPanel{

        private void updateImage() {
			
			long startTime = System.nanoTime();
			
            BigDecimal step = IMAGE_HEIGHT.divide(CORE_COUNT, MATH_CONTEXT);
            List<ImageSlicePainter> threads = new ArrayList<>();
			
            for(BigDecimal i = BigDecimal.ZERO; i.compareTo(IMAGE_HEIGHT) == -1; i = i.add(step)) {
                threads.add(new ImageSlicePainter(zoom, offsetX, offsetY, image, i, i.add(step).compareTo(IMAGE_HEIGHT) == 1 ? i.add(step) : IMAGE_HEIGHT));
				
            }
            for (ImageSlicePainter imageSlicePainter : threads) {
                imageSlicePainter.start();
            }
            try {
                for (ImageSlicePainter imageSlicePainter : threads) {
                    imageSlicePainter.join();
                }
            }
            catch (InterruptedException e) {
                e.getStackTrace();
            }
			
			long stopTime = System.nanoTime();
			System.out.println("frame render time: " + (stopTime - startTime) / 1000000000.0 + " s");
			
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g.create();
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            if(stateChanged) {
                updateImage();
                stateChanged = false;
            }
            g2d.drawImage(image, 0, 0, null);
            g2d.dispose();
        }
    }
}